﻿return
; 设置热键
/*
*/
LoadHotkey()
{
    Hotkey, RButton, GestureGo, On Useerrorlevel
}

GestureGo:
	LastGesture := MG_Recognize()	
return

; 执行手势对应动作
GZ_ActionDo(g, GZObj)
{
    ;GZObj["id"] MouseGet的 ahk_id
    ;GZObj["control"] 控件的ClassNN 
    ;GZObj["class"] 窗口的 ahk_class
    ;GZObj["x"] 初始的 x 坐标
    ;GZObj["y"] 初始的 y 坐标
    ;msgbox % g "`n" GZObj["class"]
    return
    class := GZObj["class"]
    ; 如果手势为 N ，而且当前 class 为 资源管理器(CabinetWClass)
    ; 执行对应的Label: RunNotepad
    msgbox % g "`n" class
    if (g = "n") and ( class = "CabinetWClass")
        Gosub, RunNotepad
    else if (g = "c") and ( class = "CabinetWClass")
        Gosub, RunCalc
    ;else if (g = "c")
        ;Send, !{f4}
}

MG_ReadName2()
{
    global LastGesturePos
  Loop,% LastGesturePos[0]
	{
		pos := LastGesturePos[A_Index]
		If Pos.x1 > xmax
			xmax := Pos.x1
		If Pos.x2 > xmax
			xmax := Pos.x2
		If Pos.x1 < xmin
			xmin := Pos.x1
		If Pos.x2 < xmin
			xmin := Pos.x2
		If Pos.y1 > ymax
			ymax := Pos.y1
		If Pos.y2 > ymax
			ymax := Pos.y2
		If Pos.y1 < ymin
			ymin := Pos.y1
		If Pos.y2 < ymin
			ymin := Pos.y2
    LastPos := A_Index
	}
  startx := LastGesturePos[1].x1
  starty := LastGesturePos[1].y1
  endx := Pos.x2
  endy := Pos.y2
    ; 宽与高
     w := xmax-xmin
     h := ymax-ymin
    ; 中心位置
    w2 := w/2
    h2 := h/2
    cx := xmin+w2
    cy := ymin+h2
  ;tooltip % startx/endx  "`n" starty/endy
}
RunCalc:
    run calc.exe
return
RunNotepad:
    run notepad.exe
return
